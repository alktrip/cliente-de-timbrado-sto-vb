﻿Imports System.Xml
Imports System.ServiceModel
Imports System.Security.Cryptography

Public Class Connection

    ' Credenciales de acceso, cambiar de acuerdo con el servicio (pruebas o producción)
    Private user As String = "USUARIO"
    Private password As String = "PASSWORD"
    Private urlStamp As String = "https://pac.stofactura.com/pac-massive-ws/CfdMassStamper"

    Function enviar(ByVal xml() As Byte, ByVal richTextBox As RichTextBox) As Boolean

        Dim utilities As New Utilities
        Dim response() As Byte
        Dim r As String
        Dim passMD5 As String
        Dim servicio As String = ""

        If urlStamp.Contains("https://pac.stofactura.com") Then
            servicio = "producción"
        Else
            If urlStamp.Contains("http://pac-test.stofactura.com") Then
                servicio = "pruebas"
            End If
        End If

        passMD5 = utilities.MD5EncryptPass(password)

        ' Se realiza la conexión
        System.Net.ServicePointManager.ServerCertificateValidationCallback = _
        Function(se As Object, _
        cert As System.Security.Cryptography.X509Certificates.X509Certificate, _
        chain As System.Security.Cryptography.X509Certificates.X509Chain, _
        sslerror As System.Net.Security.SslPolicyErrors) True
        System.Net.ServicePointManager.ServerCertificateValidationCallback = Nothing

        ' Se construye el Binding
        Dim securitymode As BasicHttpSecurityMode = BasicHttpSecurityMode.Transport
        Dim binding As New BasicHttpBinding(securitymode)
        binding.MaxReceivedMessageSize = Integer.MaxValue
        Dim endPointAddress As New EndpointAddress(urlStamp)
        Dim cfdMassStamper As New STO_WS.MassiveReceptionControllerImplClient(binding, endPointAddress)

        MsgBox("Se enviará el documento a timbrar al servicio de timbrado de " + servicio)

        ' Se realiza el timbrado del documento y se obtiene el response del WebService
        response = cfdMassStamper.StampCFDBytes(xml, user, passMD5)

        ' Se crea el texto a partir del response
        r = System.Text.UnicodeEncoding.UTF8.GetChars(response)

        richTextBox.Text = r

        Return True
    End Function

End Class
