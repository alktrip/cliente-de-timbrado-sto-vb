﻿Imports System.IO

Public Class Principal
    Private xml() As Byte

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OpenFileDialog1.Title = "Seleccionar CFDi versión 3.2"
        OpenFileDialog1.Filter = "CFDi v3.2|*.xml"
    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim response As MsgBoxResult
        response = MsgBox("¿Desea salir de la aplicación?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirmar")
        If response = MsgBoxResult.Yes Then
            Me.Dispose()
        ElseIf response = MsgBoxResult.No Then
            e.Cancel = True
            Exit Sub
        End If
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        FileText.Clear()
        ResponseText.Clear()

        Dim xmlBytes As New Utilities

        If OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Dim str As String = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
            Label5.Text = OpenFileDialog1.FileName
            FileText.Text = str
            xml = xmlBytes.GetStreamAsByteArray(str)
            Button1.Enabled = True
        End If
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim env As New Connection
        env.enviar(xml, ResponseText)
        Button1.Enabled = False
    End Sub

    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim response As MsgBoxResult
        response = MsgBox("¿Desea salir de la aplicación?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirmar")
        If response = MsgBoxResult.Yes Then
            Me.Dispose()
        ElseIf response = MsgBoxResult.No Then
            Exit Sub
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        FileText.Clear()
        ResponseText.Clear()
        Label5.Text = ""
    End Sub
End Class
