﻿Imports System.Security.Cryptography

Public Class Utilities
    ' Función para convertir el contenido del XML a un arreglo de bytes
    Public Function GetStreamAsByteArray(ByVal sr As String) As Byte()

        Dim fileData As Byte()

        fileData = System.Text.Encoding.UTF8.GetBytes(sr)

        Return fileData

    End Function

    ' Funcion para generar el MD5 de la contraseña
    Public Function MD5EncryptPass(ByVal StrPass As String) As String
        Dim PasConMd5 As String = ""
        Dim md5 As New MD5CryptoServiceProvider
        Dim bytValue() As Byte
        Dim bytHash() As Byte
        Dim i As Integer

        bytValue = System.Text.Encoding.UTF8.GetBytes(StrPass)

        bytHash = md5.ComputeHash(bytValue)
        md5.Clear()

        For i = 0 To bytHash.Length - 1
            PasConMd5 &= bytHash(i).ToString("x").PadLeft(2, "0")
        Next

        Return PasConMd5

    End Function

End Class
